<?php

namespace Database\Seeders;

use App\Models\Post;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Category;


class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::select('id')->get();
        $users = User::select('id')->get();
        $post = Post::factory()->times(100)->make([
            'user_id' => $users->random()->id,
            'category_id' => $categories->random()->id,

        ])->toArray();

        Post::insert($post);
    }
}

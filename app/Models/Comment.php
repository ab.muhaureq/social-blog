<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Comment extends Model
{
    use HasFactory,Uuids;


    protected $table = 'comments';
    protected $keyType = 'string';


    public $incrementing = false;


    protected $fillable = [
        'user_id',
        'post_id',
        'title',
        'context'
        ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }


    public function likes(){
        return this->morphMany(Like::class,'likeable');
    }
}

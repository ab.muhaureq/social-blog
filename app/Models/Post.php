<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory,Uuids;


    protected $table = 'posts';
    protected $keyType = 'string';


    public $incrementing = false;


    protected $fillable = [
        'user_id',
        'title',
        'summary',
        'context',
        'category_id',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function comments()
    {
        return $this->hasMany(Comment::class,);
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }


    public function category()
    {
        return $this->belongsTo(Category::class);
    }


    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function likes(){
        return this->morphMany(Like::class,'likeable');
    }
}

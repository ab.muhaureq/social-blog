<?php

use App\Http\Controllers\API\v1\SessionsController;
Route::name('sessions.')->group(function () {
Route::post('login', [SessionsController::class, 'login'])->name('login');
Route::get('test', [SessionsController::class, 'test'])->name('test');


Route::post('register', [SessionsController::class, 'register'])->name('register');
Route::get('register/activate/{token}', [SessionsController::class, 'activateAccount'])->name('activate-account');
Route::post('register/resend-activate', [SessionsController::class, 'resendActivationEmail'])->name('resend-activation-email');

/**
 * Here we put protected routes
 */
Route::middleware('auth:api')->group(function () {
    Route::post('password/change', [SessionsController::class, 'changePassword'])->name('change-password');
    Route::post('logout', [SessionsController::class, 'logout'])->name('logout');
});
});
